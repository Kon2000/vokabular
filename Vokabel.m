//
//  Vokabel.m
//  Vokabular
//
//  Created by Konrad Appel on 28.03.14.
//  Copyright (c) 2014 Konrad Appel. All rights reserved.
//

#import "Vokabel.h"

@implementation Vokabel
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init])
    {
        self.deutsch=[aDecoder decodeObjectForKey:@"deutsch"];
        self.englisch=[aDecoder decodeObjectForKey:@"englisch"];
    }
    return self;
}
                            
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:[self deutsch] forKey:@"deutsch"];
    [aCoder encodeObject:[self englisch] forKey:@"englisch"];
}
@end
