//
//  Vokabular.h
//  Vokabular
//
//  Created by Konrad Appel on 28.03.14.
//  Copyright (c) 2014 Konrad Appel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Vokabel.h"

@interface Vokabular : NSObject
-(void) addVokabel:( Vokabel *) eingang;
-(Vokabel *)getVokabel: (int) nummer;
-(NSArray *)getDeutscheVokabel: (NSString *) input;
-(int)anzahlVokabeln;
- (id)initWithCoder:(NSCoder *)aDecoder;
- (void)encodeWithCoder:(NSCoder *)coder;
@end
