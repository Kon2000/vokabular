//
//  main.m
//  Vokabular
//
//  Created by Konrad Appel on 28.03.14.
//  Copyright (c) 2014 Konrad Appel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Vokabel.h"
#import "Vokabular.h"


void sucheDeutsch(NSString *input, Vokabular *vokabular)
{
    NSArray *result;
    
    result = [vokabular getDeutscheVokabel:input];
    
    if ([result count]==0)
    {
        NSLog(@"%@ nicht gefunden", input);
    }
    else
    {
        
        for(int i=0; i<[result count];i++)
        {
            Vokabel *vokabel;
            vokabel =[result objectAtIndex:i];
            NSLog(@"%@ ist auf Englisch:  %@", input, vokabel.englisch);
        }
    }
    
}


int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        // insert code here...
        NSLog(@"Hello, World!");
        
        Vokabular *vokabular = [[Vokabular alloc]init];
        
//        Vokabel *a;
//        a= [[Vokabel alloc]init];
//        a.deutsch=@"Haus";
//        a.englisch=@"house";
//        [vokabular addVokabel:a];
//        
//        a= [[Vokabel alloc]init];
//        a.deutsch=@"Strasse";
//        a.englisch=@"street";
//        [vokabular addVokabel:a];
//
//        a= [[Vokabel alloc]init];
//        a.deutsch=@"Maus";
//        a.englisch=@"mouse";
//        [vokabular addVokabel:a];
//        
//        a= [[Vokabel alloc]init];
//        a.deutsch=@"Stadt";
//        a.englisch=@"town";
//        [vokabular addVokabel:a];
//        
//        a= [[Vokabel alloc]init];
//        a.deutsch=@"Stadt";
//        a.englisch=@"city";
//        [vokabular addVokabel:a];
        
//        a= [[Vokabel alloc]init];
//        a.deutsch=@"Stadt";
//        a.englisch=@"town,city,village";
//        [vokabular addVokabel:a];
        
        [NSKeyedUnarchiver unarchiveObjectWithFile:@"/Users/konradappel/Documents/Vokabular.txt"];

        
        NSLog(@"Das Vokabular enthält %i Vokabeln", [vokabular anzahlVokabeln]);

        sucheDeutsch(@"Haus", vokabular);
        sucheDeutsch(@"Stadt", vokabular);
        sucheDeutsch(@"Strasse", vokabular);
        sucheDeutsch(@"PLZ", vokabular);
        
        
        
        [NSKeyedArchiver archiveRootObject:vokabular toFile:@"/Users/konradappel/Documents/Vokabular.txt"];

    }
    return 0;
}

