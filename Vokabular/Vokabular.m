//
//  Vokabular.m
//  Vokabular
//
//  Created by Konrad Appel on 28.03.14.
//  Copyright (c) 2014 Konrad Appel. All rights reserved.
//

#import "Vokabular.h"

NSMutableArray *speicher;

@implementation Vokabular

- (id)init
{
    self = [super init];
    if (self) {
        speicher = [[NSMutableArray alloc]init];
    }
    return self;
}

-(void)dealloc
{

}


-(void) addVokabel:(Vokabel *)eingang
{
    [speicher addObject:eingang];
}

-(int)anzahlVokabeln
{
    return (int)[speicher count] ;
}

-(Vokabel *)getVokabel: (int) nummer
{
    if(nummer > [self anzahlVokabeln])
        return NULL;
    
    Vokabel *b;
    b=[speicher objectAtIndex: nummer-1];
    return b;
}

-(NSArray *)getDeutscheVokabel:(NSString *)input
{
    NSMutableArray *tempArray = [[NSMutableArray alloc]init];
    
    Vokabel *temp;
    
    for (int i=1; i<=[self anzahlVokabeln]; i++) {
        temp= [self getVokabel:i];
        if ([input compare:temp.deutsch]==NSOrderedSame) {
            [tempArray addObject: temp];
        }
    }
    return tempArray;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    speicher = [[NSMutableArray alloc]init];
    int anzahl = [aDecoder decodeIntForKey:@"Anzahl"];
    Vokabel *tmp;
    for( int i=0; i<anzahl;i++)
    {
        tmp= [aDecoder decodeObjectForKey:[[NSNumber numberWithInt:i+1]stringValue]];
        [speicher addObject:tmp];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeInt: [self anzahlVokabeln] forKey:@"Anzahl"];
    int i=1;
    for (Vokabel* temp in speicher) {
        [coder encodeObject:temp forKey:[[NSNumber numberWithInt:i ] stringValue]];
        i++;
    }
}
@end

