//
//  Vokabel.h
//  Vokabular
//
//  Created by Konrad Appel on 28.03.14.
//  Copyright (c) 2014 Konrad Appel. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Vokabel : NSObject
    @property NSString *deutsch;
    @property NSString *englisch;

- (id)initWithCoder:(NSCoder *)aDecoder;
- (void)encodeWithCoder:(NSCoder *)coder;
@end
